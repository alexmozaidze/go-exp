# Dotfiles status checker

A tiny program that checks my [dotfiles](https://gitlab.com/alexmozaidze/dotfiles) folder (that is managed by [stow](https://www.gnu.org/software/stow/)) for any de-synced or jammed package files.

![Demo](demo-screenshot.png)

## Usage

```sh
go run .
```

## Installation

```sh
go install
```
