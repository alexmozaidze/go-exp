package main

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
)

const (
	dotfilesPath = "dotfiles"

	packageSectionDelimiter = "---------------"
	packageFormatString     = "%s\t%s =>\t%s\n" // Icon, PackageName, PackagePath

	packageDirStatusDesynced = "📁"
	packageDirStatusOk       = "👌" // 👌

	packageFileStatusOk       = "✅"
	packageFileStatusDesynced = "🔃"
	packageFileStatusJammed   = "❌"
	packageFileStatusError    = "🚨"
)

type PackageName = string
type PackagePath = string
type PackageMap map[PackageName][]PackagePath

func (self PackageMap) insert(name PackageName, path PackagePath) {
	if self[name] == nil {
		self[name] = []PackagePath{}
	}

	self[name] = append(self[name], path)
}

func IsValidPackage(pkg fs.DirEntry) bool {
	isDotFile := pkg.Name()[0] == '.'
	isDir := pkg.Type().IsDir()

	return !isDotFile && isDir
}

func IsSymlink(fileInfo os.FileInfo) bool {
	return fileInfo.Mode()&os.ModeSymlink != 0
}

func PrintSection(pkgName, delimiter string) {
	println(delimiter, pkgName, delimiter)
}

func main() {
	homeDir, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}

	dotfiles, err := os.ReadDir(filepath.Join(homeDir, dotfilesPath))
	if err != nil {
		panic(err)
	}

	var (
		jammedPackages   = PackageMap{}
		desyncedPackages = PackageMap{}
		erroredPackages  = map[PackageName]error{}
	)

	for _, pkg := range dotfiles {
		if !IsValidPackage(pkg) {
			continue
		}

		PrintSection(pkg.Name(), packageSectionDelimiter)

		pkgContent := filepath.Join(homeDir, dotfilesPath, pkg.Name())

		basePkgPath := filepath.Join(homeDir, dotfilesPath, pkg.Name())

		isPackageName := false

		// NOTE: WalkDir ignores symlinks
		filepath.Walk(pkgContent, func(sourcePath string, sourceInfo fs.FileInfo, err error) error {
			if err != nil {
				erroredPackages[pkg.Name()] = err
				return fs.SkipAll
			}

			// First element is always the package name
			if !isPackageName {
				isPackageName = true
				return nil
			}

			relPath, err := filepath.Rel(basePkgPath, sourcePath)
			if err != nil {
				panic(err)
			}

			targetInfo, targetErr := os.Lstat(filepath.Join(homeDir, relPath))

			var (
				targetExists = false
				isSynced     = false
			)
			if targetErr == nil {
				targetExists = true
				isSynced = IsSymlink(targetInfo)
			}

			var (
				isDir  = sourceInfo.Mode().IsDir()
				isFile = sourceInfo.Mode().IsRegular()
			)

			var packageFileStatusIcon string
			defer func() {
				fmt.Printf("%s\t%s =>\t%s\n", packageFileStatusIcon, pkg.Name(), relPath)
			}()

			if isSynced && isDir {
				packageFileStatusIcon = packageDirStatusOk
				return fs.SkipDir
			} else if !isSynced && isDir {
				packageFileStatusIcon = packageDirStatusDesynced
			} else if isSynced && isFile {
				packageFileStatusIcon = packageFileStatusOk
			} else if !isSynced && isFile && !targetExists {
				packageFileStatusIcon = packageFileStatusDesynced
				desyncedPackages.insert(pkg.Name(), relPath)
			} else if !isSynced && isFile && targetExists {
				packageFileStatusIcon = packageFileStatusJammed
				jammedPackages.insert(pkg.Name(), relPath)
			}

			return nil
		})

		if erroredPackages[pkg.Name()] != nil {
			fmt.Printf(
				"%s Error encountered whilst dir walking %s\n%s\n",
				packageFileStatusError,
				packageFileStatusError,
				erroredPackages[pkg.Name()],
			)
		}
	}

	fmt.Println("Errored packages:", erroredPackages)
	fmt.Println("Jammed packages:", jammedPackages)
	fmt.Println("Desynced packages:", desyncedPackages)
}
